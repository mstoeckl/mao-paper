#!/usr/bin/env python3

from collections import defaultdict
import random

# NOTE: need to link the latest mao.gdl to the parent folder
x = [line.split(";")[0].strip() for line in 
open('../mao.gdl','r').read().split("\n")]
x = " ".join(x)
x = " ".join(k for k in x.split(" ") if k)

# Next, parse out parenthesis structure
s = [[]]
for ch in x:
    if ch == ")":
        s[-2].append(s[-1])
        del s[-1]
    elif ch == "(":
        if s[-1] and s[-1][-1] == "":
            del s[-1][-1]
        s.append([""])
    elif ch == " ":
        if s[-1][-1]:
            s[-1].append("")
    else:
        s[-1][-1] += ch

t = s[0]

def prettyprint(elem):
    if type(elem) == list:
        if elem[0] == "<=":
            s = prettyprint(elem[1]) + " \Longleftarrow " + " \land ".join(prettyprint(p) for p in elem[2:])
            return s
        elif elem[0] == "not":
            return "\lnot\! " + prettyprint(elem[1])
        elif elem[0] == "distinct":
            return prettyprint(elem[1]) + "\!\!\\ne\!\!" + prettyprint(elem[2])
        elif elem[0] == "or":
            return "\\left(" + " \lor ".join(prettyprint(p) for p in elem[1:]) + "\\right)"
        else:
            s = prettyprint(elem[0]) + "\!\left(" + ", ".join(prettyprint(p) for p in elem[1:]) + "\\right)"
            return s
    else: # string
        # handle ?, <=, etc.
        if elem[0] == "?":
            return elem[1:].upper()
        if elem in ("next","sees","random","goal","terminal","legal","true","does","init","role"):
            return "\\mathbf{" + elem + "}"
        
        return "\operatorname{"+elem+"}"

def flatten(e):
    if type(e) == list:
        x = []
        for k in e:
            x += flatten(k)
        return x
    return [e]

def treesub(e,k):
    if type(e) == list:
        return [treesub(j,k) for j in e]
    if e in k:
        return k[e]
    return e

# NOTE: sort t by category (init, next, sees, goal, terminal)

categories = defaultdict(list)
for elem in t:
    rst = True
    for j in ("init","next","sees","goal","terminal","legal","role"):
        if j in flatten(elem):
            categories[j].append(elem)
            rst = False
            break
    if rst:
        if "<=" in flatten(elem):
            categories["rule"].append(elem)
        else:
            categories["fact"].append(elem)
#print(categories)



#\mathrm{\boldsymbol{next}}\left(\mathrm{bet}\left(R,100\right)\right)\Longleftarrow\boldsymbol{\mathrm{does}}\left(\mathrm{random},\mathrm{deal}\left(C,D\right)\right)

for k,v in sorted(categories.items()):
    print(k)
    log = open("gdl/"+k+".tex","w")
    for elem in v:
        # TODO: aggressively prune unused primitives!
        if flatten(elem)[1] in ("nor","and","or","nand"):
            continue
        elif elem[0] in ("succ",):
            continue

        # May want to modify elem beforehand, i.e. to collapse variables into one letter
        symbols = {j[1:] for j in flatten(elem) if j[0]=="?"}
        conv = {}
        found = {}
        for s in sorted(list(symbols),key=lambda x:len(x)):
            k = ""
            for j in s:
                k += j
                if k not in found:
                    found[k] = None
                    conv["?"+s] = "?"+k
                    break
        unused = set("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        for k in found:
            if len(k) == 1:
                unused.remove(k.upper())
        for c in conv:
            if len(conv[c]) > 2:
                j = random.sample(unused,1)[0]
                unused.remove(j)
                conv[c] = "?"+j
        # Verbosity aids:
        conv["exists-a-greater-card"] = "exg-card"
        conv["is-greatest-held-card"] = "isg-card"
        
        conv["exists-permit-rule"] = "experm"
        conv["exists-greater-forbid-rule"] = "exgforb"
        conv["exists-forbid-rule"] = "exforb"
        conv["exists-greater-permit-rule"] = "exgperm"
        conv["exists-reverse-rule"] = "exrev"
        conv["exists-continue-rule"] = "excont"
        conv["exists-greater-reverse-rule"] = "exgrev"
        conv["exists-greater-continue-rule"] = "exgcont"
        conv["exists-skip-rule"] = "exskip"
        conv["exists-greater-skip-rule"] = "exgskip"
        
        conv["cond-match"] = "cmat"
        conv["shall-reverse"] = "willrev"
        conv["rotate-right"] = "rotr"
        conv["skip-dist"] = "skipd"
        conv["turnstate"] = "tstate"
        conv["last-card"] = "lcard"
        conv["combo-legal"] = "combleg"
        conv["combo-mustsay"] = "combsay"
        conv["combo-reverse"] = "combrev"
        conv["combo-skip"] = "combskip"
        conv["valid-place"] = "valplace"
        conv["illegal-place"] = "ivlplace"
        conv["next-id"] = "nid"
        conv["noop-ing"] = "noping"
        conv["is-random-busy"] = "randbusy"
        conv["exists-bigger-id"] = "exgid"
        conv["hand-id"] = "hid"
        conv["has-moved"] = "moved"
        conv["turn-ending"] = "eot"
        conv["movenumber"] = "moveno"
        conv["hand-not-full"] = "notfull"
        conv["forgot-to-say"] = "pensay"
        conv["out-of-turn"] = "ninturn"
        conv["hand-order"] = "hsucc"
        conv["sayable"] = "cansay"
        conv["diamonds"] = "\\diamondsuit"
        conv["spades"] = "\\spadesuit"
        conv["hearts"] = "\\heartsuit"
        conv["clubs"] = "\\clubsuit"
        conv["player1"] = "play1"
        conv["player2"] = "play2"
        conv["player3"] = "play3"
        conv["player4"] = "play4"
        conv["player5"] = "play5"
        conv["j1"] = "nocard"
                
        # While true, reduce set to monochar
        
        elem = treesub(elem, conv)
        
        k = prettyprint(elem)
        print(file=log)
        print("$ "+k+" $", file=log)

    
# TODO: be sure to note GDL augmented with and/or/not; and dataset of succ(X,Y), hand-order, etc.
