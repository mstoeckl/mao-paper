#!/bin/sh

pushd build
ln -s ../figures .
ln -s ../main.tex .
ln -s ../ref.bib .
ln -s ../gdl .
latex main.tex
bibtex main
pdflatex main.tex
popd
ln -sf build/main.pdf .
